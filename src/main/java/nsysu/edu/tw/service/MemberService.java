package nsysu.edu.tw.service;

import org.springframework.http.ResponseEntity;

import nsysu.edu.tw.model.request.AddRegisterRequest;
import nsysu.edu.tw.model.request.CheckIdentityRequest;
import nsysu.edu.tw.model.response.AddRegisterResponse;
import nsysu.edu.tw.model.response.CheckIdentityResponse;

public interface MemberService {

	ResponseEntity<AddRegisterResponse> addRegister(AddRegisterRequest req);
	
	ResponseEntity<CheckIdentityResponse> checkIdentity(CheckIdentityRequest req);
	
}
