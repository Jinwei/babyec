package nsysu.edu.tw.service;

import org.springframework.http.ResponseEntity;

import nsysu.edu.tw.model.response.AddCartResponse;
import nsysu.edu.tw.model.response.CartListResponse;

public interface CartService {

	
	ResponseEntity<AddCartResponse> addCart(String pNo,String mId);
	
	ResponseEntity<CartListResponse> getCartList(String mId);
	
	ResponseEntity<CartListResponse> delCart(String pNo,String mId);
}
