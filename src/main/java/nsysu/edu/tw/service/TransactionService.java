package nsysu.edu.tw.service;

import org.springframework.http.ResponseEntity;

import nsysu.edu.tw.model.request.ConfirmRequest;
import nsysu.edu.tw.model.request.SaveTransactionRequest;
import nsysu.edu.tw.model.response.BuyerDataResponse;
import nsysu.edu.tw.model.response.BuyerListResponse;
import nsysu.edu.tw.model.response.SaveTransactionResponse;
import nsysu.edu.tw.model.response.SellerListResponse;

public interface TransactionService {

	ResponseEntity<SellerListResponse> getSellerList(String mId);
	
	ResponseEntity<SellerListResponse> getSellerAllList(String mId);
	
	ResponseEntity<SaveTransactionResponse> saveTransaction(SaveTransactionRequest request);
	
	ResponseEntity<BuyerListResponse> getBuyerList(String mId);
	
	ResponseEntity<BuyerDataResponse> comfirm (ConfirmRequest request);
	
	ResponseEntity<BuyerDataResponse> refund(ConfirmRequest request);
}
