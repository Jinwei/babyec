package nsysu.edu.tw.service;

import org.springframework.http.ResponseEntity;

import nsysu.edu.tw.model.request.AddProductRequest;
import nsysu.edu.tw.model.response.AddProductResponse;
import nsysu.edu.tw.model.response.ProductListResponse;
import nsysu.edu.tw.model.response.ProductResponse;

public interface ProductService {

	ResponseEntity<AddProductResponse> addProduct(AddProductRequest request);
	
	ResponseEntity<ProductResponse> getProduct(String pNo);

	ResponseEntity<ProductListResponse> getCidProductList(String pCid);
	
	ResponseEntity<ProductListResponse> getBidProductList(String pBid);
}
