package nsysu.edu.tw.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public abstract class AbstractService {

	protected String getUUID() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replaceAll("-", "");
		uuid = uuid.substring(uuid.length() - 10);
		return uuid;

	}


	public static final Pattern EMAIL_PATTERN = Pattern.compile("^\\w+\\.*\\w+@(\\w+\\.){1,5}[a-zA-Z]{2,3}$");
	/**
	 * Email �榡�ˬd�{��
	 * 
	 **/
	public static boolean isValidEmail(String email) {
		boolean result = false;
		if (EMAIL_PATTERN.matcher(email).matches()) {
			result = true;
		}
		return result;
	}
	
	
	public void sendMail(String mailTo) {  
        String host = "smtp.gmail.com";  
        int port = 587;  
        final String username = "jinwei7496@gmail.com";  
        final String password = "Xx108314";  
  
        Properties props = new Properties();  
        props.put("mail.smtp.host", host);  
        props.put("mail.smtp.auth", "true");  
        props.put("mail.smtp.starttls.enable", "true");  
        props.put("mail.smtp.port", port);  
          
        Session session = Session.getInstance(props,new Authenticator(){  
              protected PasswordAuthentication getPasswordAuthentication() {  
                  return new PasswordAuthentication(username, password);  
              }} );  
           
        try {  
  
        Message message = new MimeMessage(session);  
        message.setFrom(new InternetAddress(username));  
        message.setRecipients(Message.RecipientType.TO,   
                        InternetAddress.parse(mailTo));  
        message.setSubject("Testing Subject");  
        message.setText("Dear Mail Crawler,\n\n No spam to my email, please!");  
  
        Transport transport = session.getTransport("smtp");  
        transport.connect(host, port, username, password);  
  
        Transport.send(message);  
  
        System.out.println("Done");  
  
        } catch (MessagingException e) {  
            throw new RuntimeException(e);  
        }  
    }  
	
	/**
	 * ���o��e�ɶ� �榡 YYYY/MM/DD HH:mm:ss
	 * @return
	 */
	public String getCurrentTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String date = sdf.format(new Date(System.currentTimeMillis()));
		return date;
	}
	
	/**
	 * �榡�Ʈɶ� �榡 YYYY/MM/DD HH:mm:ss
	 * @return
	 */
	public String getCurrentTime(Long timeMillis){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String date = sdf.format(new Date(timeMillis));
		return date;
	}
}
