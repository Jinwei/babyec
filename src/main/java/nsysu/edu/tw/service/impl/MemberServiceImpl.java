package nsysu.edu.tw.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import nsysu.edu.tw.dao.MemberDao;
import nsysu.edu.tw.model.request.AddRegisterRequest;
import nsysu.edu.tw.model.request.CheckIdentityRequest;
import nsysu.edu.tw.model.response.AddRegisterResponse;
import nsysu.edu.tw.model.response.CheckIdentityResponse;
import nsysu.edu.tw.model.table.Member;
import nsysu.edu.tw.service.AbstractService;
import nsysu.edu.tw.service.MemberService;

@Component
public class MemberServiceImpl extends AbstractService implements MemberService{

	@Autowired
	private MemberDao memberDao;
	
	@Override
	public ResponseEntity<AddRegisterResponse> addRegister(AddRegisterRequest req) {
		AddRegisterResponse response = new AddRegisterResponse();
		Member member = null;
		
		member = memberDao.findByEmail(req.geteMail());
		
		if(member == null){
			String mId = getUUID();
			
			member = new Member();
			member.setmId(mId);
			member.setEmail(req.geteMail());
			member.setmName(req.getmName());
			member.setPassword(req.getPwd());
			member.setPhone(req.getPhone());
			memberDao.saveCustomer(member);
			
//			sendMail(req.geteMail());
			
			response.setStatus("Success");
		}else{
			response.setStatus("Error");
			response.setMsg("帳號已存在!");
		}
		return new ResponseEntity<AddRegisterResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CheckIdentityResponse> checkIdentity(CheckIdentityRequest req) {
		CheckIdentityResponse response = new CheckIdentityResponse();
		Member member = null;
		member = memberDao.checkIdetity(req.geteMail(), req.getPwd());
		if(member == null){
			response.setStatus("Error");
			response.setMsg("帳號或密碼錯誤!");
		}else{
			response.setStatus("Success");
			response.setmId(member.getmId());
			
		}
		
		return new ResponseEntity<CheckIdentityResponse>(response, HttpStatus.OK);
	}
	
	

}
