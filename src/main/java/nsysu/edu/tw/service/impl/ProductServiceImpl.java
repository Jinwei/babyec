package nsysu.edu.tw.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import nsysu.edu.tw.dao.MemberDao;
import nsysu.edu.tw.dao.ProductDao;
import nsysu.edu.tw.dao.RecordDao;
import nsysu.edu.tw.model.request.AddProductRequest;
import nsysu.edu.tw.model.response.AddProductResponse;
import nsysu.edu.tw.model.response.ProductData;
import nsysu.edu.tw.model.response.ProductListResponse;
import nsysu.edu.tw.model.response.ProductResponse;
import nsysu.edu.tw.model.table.Member;
import nsysu.edu.tw.model.table.Product;
import nsysu.edu.tw.model.table.Record;
import nsysu.edu.tw.service.AbstractService;
import nsysu.edu.tw.service.ProductService;

@Component
public class ProductServiceImpl extends AbstractService implements ProductService {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private RecordDao recordDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Override
	public ResponseEntity<AddProductResponse> addProduct(AddProductRequest request) {
		AddProductResponse response = new AddProductResponse();
		Product product = new Product();
		Member member = memberDao.findByMId(request.getmId());
		product.setpNo(getUUID());
		product.setMember(member);
//		product.setpMid(request.getmId());
		product.setpName(request.getpName());
		product.setpDescription(request.getpDesc());
		product.setpPicture(request.getpPicture());
		product.setUnitPrice(request.getpPrice());
		product.setCatalog(request.getpCid());
		product.setBrand(request.getpBid());
		
		productDao.saveProduct(product);
		
		response.setStatus("Success");
		response.setMsg("新增成功");
		
		
		return new ResponseEntity<AddProductResponse>(response, HttpStatus.OK);
	}


	@Override
	public ResponseEntity<ProductResponse> getProduct(String pNo) {

		Product product = productDao.findByPNo(pNo);
		
		ProductResponse response = new ProductResponse();
		
		ProductData productData = new ProductData();
		productData.setpMid(product.getMember().getmId());
		productData.setpMname(product.getMember().getmName());
		productData.setpNo(product.getpNo());
		productData.setpName(product.getpName());
		productData.setBrand(product.getBrand());
		productData.setCatalog(product.getCatalog());
		productData.setpDescription(product.getpDescription());
		productData.setpPicture(product.getpPicture());
		productData.setUnitPrice(product.getUnitPrice());
		
		
		response.setStatus("Success");
		response.setProduct(productData);
		
		return new ResponseEntity<ProductResponse>(response, HttpStatus.OK);
	}


	@Override
	public ResponseEntity<ProductListResponse> getCidProductList(String pCid) {
		ProductListResponse response = new ProductListResponse();
		List<Product> allProduct = productDao.findProductByCid(pCid);
		
		List<Product> productList = new ArrayList<Product>();
		for(Product product : allProduct){
			List<Record> recordList = recordDao.findRecordByPNo(product.getpNo());
			if(recordList == null || recordList.size()==0){
				productList.add(product);
			}
		}
		response.setStatus("Success");
		response.setProductList(productList);
		
		return new ResponseEntity<ProductListResponse>(response, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<ProductListResponse> getBidProductList(String pBid) {
		ProductListResponse response = new ProductListResponse();
		List<Product> allProduct = productDao.findProductByBid(pBid);
		
		List<Product> productList = new ArrayList<Product>();
		for(Product product : allProduct){
			List<Record> recordList = recordDao.findRecordByPNo(product.getpNo());
			if(recordList == null || recordList.size()==0){
				productList.add(product);
			}
		}
		response.setStatus("Success");
		response.setProductList(productList);
		
		return new ResponseEntity<ProductListResponse>(response, HttpStatus.OK);
	}

}
