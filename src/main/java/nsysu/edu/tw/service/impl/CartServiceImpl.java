package nsysu.edu.tw.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import nsysu.edu.tw.dao.CartDao;
import nsysu.edu.tw.dao.MemberDao;
import nsysu.edu.tw.dao.OrderDao;
import nsysu.edu.tw.dao.ProductDao;
import nsysu.edu.tw.model.response.AddCartResponse;
import nsysu.edu.tw.model.response.CartData;
import nsysu.edu.tw.model.response.CartListResponse;
import nsysu.edu.tw.model.table.Cart;
import nsysu.edu.tw.model.table.Member;
import nsysu.edu.tw.model.table.Order;
import nsysu.edu.tw.model.table.Product;
import nsysu.edu.tw.service.AbstractService;
import nsysu.edu.tw.service.CartService;

@Component
public class CartServiceImpl extends AbstractService implements CartService {

	@Autowired
	private CartDao cartDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Override
	public ResponseEntity<AddCartResponse> addCart(String pNo, String mId) {
		Cart cart = cartDao.findNewCartByMid(mId);
		Member member = memberDao.findByMId(mId);
		String currentTime = "";
		if(cart == null){
			currentTime = getCurrentTime();
			cart = new Cart();
			cart.setMember(member);
//			cart.setmId(mId);
			cart.setCartTime(currentTime);
//			cart.settNo("");
			cartDao.saveCart(cart);
		}else{
			currentTime = cart.getCartTime();
		}
		Product product = productDao.findByPNo(pNo);
		Order order = new Order();
		order.setMember(member);
//		order.setoMid(mId);
//		order.setCartTime(currentTime);
//		order.setpNo(pNo);
		order.setoPrice(product.getUnitPrice());
		order.setCart(cart);
		order.setProduct(product);
		
		order.setAmount(1);
		
		orderDao.saveOrder(order);
		
		List<Order> orderList = orderDao.findOrderByMidAndCartTime(mId, currentTime);
		
		AddCartResponse response = new AddCartResponse();
		response.setStatus("Success");
		if(orderList != null){
			response.setpCount(orderList.size());
		}
		
		return new ResponseEntity<AddCartResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CartListResponse> getCartList(String mId) {
		Cart cart = cartDao.findNewCartByMid(mId);
		CartListResponse response = new CartListResponse();
//		List<Cart> carts = cartDao.findCartByMId(mId);
//		for(Cart cart : carts){
//			if(cart.getTransaction() == null){
//				List<Order> orderList = orderDao.findOrderByMidAndCartTime(mId, cart.getCartTime());
//				response.setStatus("Success");
//				if(orderList != null && orderList.size() > 0){
//					List<CartData> cartList = new ArrayList<CartData>();
//					for(Order order : orderList){
//						Product product = order.getProduct();
////					Product product = productDao.findByPNo(order.getpNo());
//						CartData cartData = new CartData();
//						cartData.setmId(mId);
//						cartData.setpNo(product.getpNo());
//						cartData.setpName(product.getpName());
//						cartData.setpPrice(product.getUnitPrice());
//						cartList.add(cartData);
//					}
//					
//					response.setCartList(cartList);
//				}
//			}
			
			if(cart == null){
				response.setStatus("Success");
				response.setMsg("購物車無資料");
			}else{
				List<Order> orderList = orderDao.findOrderByMidAndCartTime(mId, cart.getCartTime());
				response.setStatus("Success");
				if(orderList != null && orderList.size() > 0){
					List<CartData> cartList = new ArrayList<CartData>();
					for(Order order : orderList){
						Product product = order.getProduct();
//					Product product = productDao.findByPNo(order.getpNo());
						CartData cartData = new CartData();
						cartData.setmId(mId);
						cartData.setpNo(product.getpNo());
						cartData.setpName(product.getpName());
						cartData.setpPrice(product.getUnitPrice());
						cartList.add(cartData);
					}
					
					response.setCartList(cartList);
				}else{
					response.setMsg("購物車無資料");
				}
			}
			
//		}
		
		return new ResponseEntity<CartListResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CartListResponse> delCart(String pNo, String mId) {
		Cart cart = cartDao.findNewCartByMid(mId);
		CartListResponse response = new CartListResponse();
		if(cart == null){
			response.setStatus("Success");
			response.setMsg("購物車無資料");
		}else{
			Order order = orderDao.findByPNo(pNo);
			if(order != null){
				orderDao.deleteOrder(order.getSeq());
			}else{
				response.setStatus("Success");
				response.setMsg("購物車中無此商品");
			}
			
			List<Order> orderList = orderDao.findOrderByMidAndCartTime(mId, cart.getCartTime());
			response.setStatus("Success");
			if(orderList != null && orderList.size() > 0){
				List<CartData> cartList = new ArrayList<CartData>();
				for(Order od : orderList){
					Product product = od.getProduct();
//					Product product = productDao.findByPNo(od.getpNo());
					CartData cartData = new CartData();
					cartData.setmId(mId);
					cartData.setpNo(product.getpNo());
					cartData.setpName(product.getpName());
					cartData.setpPrice(product.getUnitPrice());
					cartList.add(cartData);
				}
				
				response.setCartList(cartList);
			}else{
				response.setMsg("購物車無資料");
			}
		}
		
		return new ResponseEntity<CartListResponse>(response, HttpStatus.OK);
	}

}
