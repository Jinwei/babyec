package nsysu.edu.tw.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import nsysu.edu.tw.dao.CartDao;
import nsysu.edu.tw.dao.MemberDao;
import nsysu.edu.tw.dao.OrderDao;
import nsysu.edu.tw.dao.ProductDao;
import nsysu.edu.tw.dao.RecordDao;
import nsysu.edu.tw.dao.TransactionDao;
import nsysu.edu.tw.model.request.ConfirmRequest;
import nsysu.edu.tw.model.request.SaveTransactionRequest;
import nsysu.edu.tw.model.response.BuyerData;
import nsysu.edu.tw.model.response.BuyerDataResponse;
import nsysu.edu.tw.model.response.BuyerListResponse;
import nsysu.edu.tw.model.response.SaveTransactionResponse;
import nsysu.edu.tw.model.response.SellerData;
import nsysu.edu.tw.model.response.SellerListResponse;
import nsysu.edu.tw.model.table.Cart;
import nsysu.edu.tw.model.table.Member;
import nsysu.edu.tw.model.table.Order;
import nsysu.edu.tw.model.table.Product;
import nsysu.edu.tw.model.table.Record;
import nsysu.edu.tw.model.table.Transaction;
import nsysu.edu.tw.service.AbstractService;
import nsysu.edu.tw.service.TransactionService;

@Component
public class TransactionServiceImpl extends AbstractService implements TransactionService {

	@Autowired
	private TransactionDao transactionDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired 
	private RecordDao recordDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CartDao cartDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Override
	public ResponseEntity<SellerListResponse> getSellerList(String mId) {
		SellerListResponse response = new SellerListResponse();
		List<SellerData> sellerList = new ArrayList<SellerData>();
		
		List<Product> productList = productDao.findProductByMid(mId);
		
		for(Product product : productList){
			Order order = orderDao.findByPNo(product.getpNo());
			System.out.println(order);
			if(order != null){
				Cart cart = order.getCart();
//				Cart cart = cartDao.findCartByCartTime(order.getCartTime());
				System.out.println(cart);
				if(cart != null){
					Transaction transaction = cart.getTransaction();
//					Transaction transaction = transactionDao.findByTNo(cart.gettNo());
					if(transaction != null){
						Member member = transaction.getMember();
//						Member member  = memberDao.findByMId(transaction.getTransMid());
						
						SellerData sellerData = new SellerData();
						
						sellerData.setTransMid(member.getmId());
						sellerData.setmName(member.getmName());
						sellerData.setPname(product.getpName());
						sellerData.setTransTime(transaction.getTransTime());
						sellerData.setTransStatus(transaction.getTransStatus());
						sellerData.setpNo(product.getpNo());
						sellerData.setpPicture(product.getpPicture());
						
						sellerList.add(sellerData);
					}
				}
			}
		}
		response.setStatus("Success");
		response.setSellerList(sellerList);
		return new ResponseEntity<SellerListResponse>(response, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<SellerListResponse> getSellerAllList(String mId) {
		SellerListResponse response = new SellerListResponse();
		List<SellerData> sellerList = new ArrayList<SellerData>();
		
		List<Product> productList = productDao.findProductByMid(mId);
		for(Product product : productList){
			SellerData sellerData = new SellerData();
			
			sellerData.setPname(product.getpName());
			sellerData.setpPicture(product.getpPicture());
			sellerData.setoPrice(product.getUnitPrice());
			sellerData.setpNo(product.getpNo());
			
			sellerList.add(sellerData);
		}
		response.setStatus("Success");
		response.setSellerList(sellerList);
		return new ResponseEntity<SellerListResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<SaveTransactionResponse> saveTransaction(SaveTransactionRequest request) {
		SaveTransactionResponse response = new SaveTransactionResponse();
		Cart cart = cartDao.findNewCartByMid(request.getmId());
		List<Order> orders = orderDao.findOrderByMidAndCartTime(request.getmId(), cart.getCartTime());
		for(String pNo : request.getpNo()){
			Order order = orderDao.findOrderByMidAndCartTimeAndPNo(request.getmId(), cart.getCartTime(), pNo);
			if(order == null){
				Product product = productDao.findByPNo(pNo);
				
				Order newOrder = new Order();
				newOrder.setCart(cart);
				newOrder.setMember(cart.getMember());
				newOrder.setProduct(product);
				newOrder.setoPrice(product.getUnitPrice());
				newOrder.setAmount(1);
				
				orderDao.saveOrder(newOrder);
			}
			
			
			
		}
		for(Order order : orders){
			Product product = order.getProduct();
			if(!request.getpNo().contains(product.getpNo())){
				Cart newCart = new Cart();
				newCart.setCartTime(getCurrentTime());
				newCart.setMember(cart.getMember());
				
				cartDao.saveCart(newCart);
				
				Order newOrder = new Order();
				newOrder.setCart(newCart);
				newOrder.setMember(cart.getMember());
				newOrder.setProduct(product);
				newOrder.setoPrice(product.getUnitPrice());
				newOrder.setAmount(1);
				
				orderDao.saveOrder(newOrder);
				
				orderDao.deleteOrder(order.getSeq());
			}
		}
		
		
		Transaction transaction = new Transaction();
		transaction.setCart(cart);
		transaction.setMember(cart.getMember());
		transaction.setpStore(request.getpStore());
		transaction.settNo(getUUID());
		transaction.setTransTime(getCurrentTime());
		transaction.setTransStatus("0");
		transaction.setTransLocation(request.getpStore());
		
		transactionDao.saveTransaction(transaction);
		
		cart.setTransaction(transaction);
		
		cartDao.updateCart(cart);
		
		for(String pNo : request.getpNo()){
			Record record = new Record();
			record.setTransaction(transaction);
			record.setpNo(pNo);
			record.setAmount(1);
			
			recordDao.saveCart(record);
		}
		
		
		response.setStatus("Success");
		response.setStatus("交易完成");
		return new ResponseEntity<SaveTransactionResponse>(response, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<BuyerListResponse> getBuyerList(String mId) {
		BuyerListResponse response = new BuyerListResponse();
		
		List<Cart> carts = cartDao.findCartByMId(mId);
		
		List<BuyerData> buyerList = new ArrayList<BuyerData>();
		
		for(Cart cart : carts){
			if(cart.getTransaction() != null){
				List<Order> orders = orderDao.findOrderByMidAndCartTime(mId, cart.getCartTime());
				Transaction transaction = cart.getTransaction();
				
				for(Order order : orders){
					Product product = order.getProduct();
					Member transMember = product.getMember();
					BuyerData data = new BuyerData();
					data.setTransMid(transMember.getmId());
					data.setmName(transMember.getmName());
					data.setPname(product.getpName());
					data.setoPrice(order.getoPrice());
					data.setTransStatus(transaction.getTransStatus());
					data.setTransTime(transaction.getTransTime());
					data.setpPicture(product.getpPicture());
					data.setpNo(product.getpNo());
					
					buyerList.add(data);
				}
			}
		}
		
		
		response.setStatus("Success");
		response.setBuyerList(buyerList);
		return new ResponseEntity<BuyerListResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<BuyerDataResponse> comfirm(ConfirmRequest request) {
		List<Cart> carts = cartDao.findCartByMId(request.getmId());
		BuyerDataResponse response = new BuyerDataResponse();
		BuyerData data = null;
		for(Cart cart : carts){
			if(cart.getTransaction() != null){
				Order order = orderDao.findOrderByMidAndCartTimeAndPNo(request.getmId(), cart.getCartTime(), request.getpNo());
				if(order != null){
					Transaction trans = transactionDao.findByTNo(cart.getTransaction().gettNo());
					
					trans.setTransStatus("3");
					transactionDao.updateTransaction(trans);
					Product product = order.getProduct();
					Member transMember = product.getMember();
					data = new BuyerData();
					
					data.setTransMid(transMember.getmId());
					data.setmName(transMember.getmName());
					data.setPname(product.getpName());
					data.setoPrice(order.getoPrice());
					data.setpPicture(product.getpPicture());
					data.setpNo(product.getpNo());
					
					data.setTransStatus(trans.getTransStatus());
					data.setTransTime(trans.getTransTime());
				}
			}
		}
		
		if(data!= null){
			response.setStatus("Success");
			response.setBuyerData(data);
			response.setStatus("完成收貨確認");
		}else{
			response.setStatus("fail");
			response.setMsg("查無資料");
		}
		
		return new ResponseEntity<BuyerDataResponse>(response, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<BuyerDataResponse> refund(ConfirmRequest request) {
		List<Cart> carts = cartDao.findCartByMId(request.getmId());
		BuyerDataResponse response = new BuyerDataResponse();
		BuyerData data = null;
		for(Cart cart : carts){
			if(cart.getTransaction() != null){
				Order order = orderDao.findOrderByMidAndCartTimeAndPNo(request.getmId(), cart.getCartTime(), request.getpNo());
				if(order != null){
					Transaction trans = transactionDao.findByTNo(cart.getTransaction().gettNo());
					
					trans.setTransStatus("4");
					transactionDao.updateTransaction(trans);
					Product product = order.getProduct();
					Member transMember = product.getMember();
					data = new BuyerData();
					
					data.setTransMid(transMember.getmId());
					data.setmName(transMember.getmName());
					data.setPname(product.getpName());
					data.setoPrice(order.getoPrice());
					data.setpPicture(product.getpPicture());
					data.setpNo(product.getpNo());
					
					data.setTransStatus(trans.getTransStatus());
					data.setTransTime(trans.getTransTime());
				}
			}
		}
		
		if(data!= null){
			response.setStatus("Success");
			response.setBuyerData(data);
			response.setStatus("退貨完成");
		}else{
			response.setStatus("fail");
			response.setMsg("查無資料");
		}
		
		return new ResponseEntity<BuyerDataResponse>(response, HttpStatus.OK);
	}
}
