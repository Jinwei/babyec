package nsysu.edu.tw.dao;

import java.util.List;

import nsysu.edu.tw.model.table.Member;


public interface MemberDao {
	
	void saveCustomer(Member member);
    
    List<Member> findAllCustomer();
     
    void deleteCustomer(Integer id);
     
    Member findByid(Integer id);
    
    Member findByName(String name);
    
    Member findByEmail(String email);
    
    Member checkIdetity(String email, String password);
    
    Member findByMId(String mId);
     
    void updateCustomer(Member member);
}
