package nsysu.edu.tw.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nsysu.edu.tw.dao.AbstractDao;
import nsysu.edu.tw.dao.RecordDao;
import nsysu.edu.tw.model.table.Record;
import nsysu.edu.tw.model.table.Transaction;

@Repository("RecordDao")
@Transactional
public class RecordDaoImpl extends AbstractDao implements RecordDao {

	@Override
	public void saveCart(Record record) {
		getSession().save(record);
	}

	@Override
	public List<Record> findAllRecord() {
		Criteria criteria = getSession().createCriteria(Transaction.class);
		return (List<Record>) criteria.list();
	}

	@Override
	public List<Record> findRecordByPNo(String pNo) {
		Criteria criteria = getSession().createCriteria(Record.class);
		criteria.add(Restrictions.eq("pNo", pNo));
		return (List<Record>) criteria.list();
	}

	@Override
	public List<Record> findRecordByTNo(String tNo) {
		Criteria criteria = getSession().createCriteria(Record.class);
		criteria.add(Restrictions.eq("transaction.tNo", tNo));
		return (List<Record>) criteria.list();
	}

	@Override
	public void deleteRecord(Integer id) {
		Query query = getSession().createSQLQuery(
				"delete from record where seq = :seq");
		query.setInteger("seq", id);
		query.executeUpdate();
	}

	@Override
	public Record findByid(Integer id) {
		Criteria criteria = getSession().createCriteria(Record.class);
		criteria.add(Restrictions.eq("seq", id));
		return (Record) criteria.uniqueResult();
	}

	@Override
	public void updateOrder(Record record) {
		getSession().update(record);
	}

}
