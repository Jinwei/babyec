package nsysu.edu.tw.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nsysu.edu.tw.dao.AbstractDao;
import nsysu.edu.tw.dao.CartDao;
import nsysu.edu.tw.model.table.Cart;

@Repository("CartDao")
@Transactional
public class CartDaoImpl extends AbstractDao implements CartDao {

	@Override
	public void saveCart(Cart cart) {
		getSession().save(cart);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cart> findAllCart() {
		Criteria criteria = getSession().createCriteria(Cart.class);
		return (List<Cart>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cart> findCartByMId(String mId) {
		Criteria criteria = getSession().createCriteria(Cart.class);
		criteria.add(Restrictions.eq("member.mId", mId));
		return (List<Cart>) criteria.list();
	}

	
	@Override
	public Cart findCartByCartTime(String cartTime) {
		Criteria criteria = getSession().createCriteria(Cart.class);
		criteria.add(Restrictions.eq("cartTime", cartTime));
		return (Cart) criteria.uniqueResult();
	}
	
	@Override
	public Cart findCartByTNo(String tNo) {
		Criteria criteria = getSession().createCriteria(Cart.class);
		criteria.add(Restrictions.eq("transaction.tNo", tNo));
		return (Cart) criteria.uniqueResult();
	}

	@Override
	public Cart findNewCartByMid(String mId) {
		Criteria criteria = getSession().createCriteria(Cart.class);
		criteria.add(Restrictions.eq("member.mId", mId));
//		criteria.add(Restrictions.eq("transaction.tNo", null));
		criteria.add(Restrictions.isNull("transaction.tNo"));
		return (Cart) criteria.uniqueResult();
	}
	
	@Override
	public void deleteRecord(Integer id) {
		Query query = getSession().createSQLQuery(
				"delete from cart where seq = :seq");
		query.setInteger("seq", id);
		query.executeUpdate();
	}

	@Override
	public Cart findByid(Integer id) {
		Criteria criteria = getSession().createCriteria(Cart.class);
		criteria.add(Restrictions.eq("seq", id));
		return (Cart) criteria.uniqueResult();
	}

	@Override
	public void updateCart(Cart cart) {
		getSession().update(cart);
	}

}
