package nsysu.edu.tw.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nsysu.edu.tw.dao.AbstractDao;
import nsysu.edu.tw.dao.MemberDao;
import nsysu.edu.tw.model.table.Member;

@Repository("MemberDao")
@Transactional
public class MemberDaoImpl extends AbstractDao implements MemberDao {

	@Override
	public void saveCustomer(Member member){
		getSession().save(member);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Member> findAllCustomer() {
		Criteria criteria = getSession().createCriteria(Member.class);
		return (List<Member>) criteria.list();
	}

	@Override
	public void deleteCustomer(Integer id) {
		Query query = getSession().createSQLQuery(
				"delete from member where seq = :seq");
		query.setInteger("seq", id);
		query.executeUpdate();
	}

	@Override
	public Member findByid(Integer id) {
		Criteria criteria = getSession().createCriteria(Member.class);
		criteria.add(Restrictions.eq("seq", id));
		return (Member) criteria.uniqueResult();
	}

	@Override
	public Member findByName(String name) {
		Criteria criteria = getSession().createCriteria(Member.class);
		criteria.add(Restrictions.eq("mName", name));
		return (Member) criteria.uniqueResult();
	}
	
	@Override
	public Member findByEmail(String email) {
		Criteria criteria = getSession().createCriteria(Member.class);
		criteria.add(Restrictions.eq("email", email));
		return (Member) criteria.uniqueResult();
	}

	@Override
	public void updateCustomer(Member member) {
		getSession().update(member);
	}
	
	@Override
	public Member findByMId(String mId) {
		Criteria criteria = getSession().createCriteria(Member.class);
		criteria.add(Restrictions.eq("mId", mId));
		return (Member) criteria.uniqueResult();
	}

	@Override
	public Member checkIdetity(String email, String password) {
		Criteria criteria = getSession().createCriteria(Member.class);
		criteria.add(Restrictions.eq("email", email));
		criteria.add(Restrictions.eq("password", password));
		return (Member) criteria.uniqueResult();
	}

	

}
