package nsysu.edu.tw.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nsysu.edu.tw.dao.AbstractDao;
import nsysu.edu.tw.dao.ProductDao;
import nsysu.edu.tw.model.table.Product;

@Repository("ProducDao")
@Transactional
public class ProductDaoImpl extends AbstractDao implements ProductDao {

	@Override
	public void saveProduct(Product product) {
		getSession().save(product);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findAllProduct() {
		Criteria criteria = getSession().createCriteria(Product.class);
		return (List<Product>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findProductByMid(String mId) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("member.mId", mId));
		return (List<Product>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findProductByCid(String cId) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("catalog", cId));
		return (List<Product>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findProductByBid(String bId) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("brand", bId));
		return (List<Product>) criteria.list();
	}

	@Override
	public void deleteProduct(Integer id) {
		Query query = getSession().createSQLQuery(
				"delete from product where seq = :seq");
		query.setInteger("seq", id);
		query.executeUpdate();
	}
	
	@Override
	public Product findByid(Integer id) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("seq", id));
		return (Product) criteria.uniqueResult();
	}
	
	@Override
	public Product findByPNo(String pNo) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("pNo", pNo));
		return (Product) criteria.uniqueResult();
	}

	@Override
	public void updateProduct(Product product) {
		getSession().update(product);
	}


}
