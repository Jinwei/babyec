package nsysu.edu.tw.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nsysu.edu.tw.dao.AbstractDao;
import nsysu.edu.tw.dao.TransactionDao;
import nsysu.edu.tw.model.table.Transaction;

@Repository("TransactionDao")
@Transactional
public class TransactionDaoImpl extends AbstractDao implements TransactionDao {

	@Override
	public void saveTransaction(Transaction transaction) {
		getSession().save(transaction);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> findAllTransaction() {
		Criteria criteria = getSession().createCriteria(Transaction.class);
		return (List<Transaction>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> findTransactionByTransMid(String transMid) {
		Criteria criteria = getSession().createCriteria(Transaction.class);
		criteria.add(Restrictions.eq("member.mId", transMid));
		return (List<Transaction>) criteria.list();
	}


	@Override
	public void updateTransaction(Transaction transaction) {
		getSession().update(transaction);
	}

	@Override
	public Transaction findByid(Integer id) {
		Criteria criteria = getSession().createCriteria(Transaction.class);
		criteria.add(Restrictions.eq("seq", id));
		return (Transaction) criteria.uniqueResult();
	}

	@Override
	public Transaction findByTNo(String tNo) {
		Criteria criteria = getSession().createCriteria(Transaction.class);
		criteria.add(Restrictions.eq("tNo", tNo));
		return (Transaction) criteria.uniqueResult();
	}

}
