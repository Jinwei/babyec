package nsysu.edu.tw.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nsysu.edu.tw.dao.AbstractDao;
import nsysu.edu.tw.dao.OrderDao;
import nsysu.edu.tw.model.table.Cart;
import nsysu.edu.tw.model.table.Order;
import nsysu.edu.tw.model.table.Product;
import nsysu.edu.tw.model.table.Transaction;

@Repository("OrderDao")
@Transactional
public class OrderDaoImpl extends AbstractDao implements OrderDao{

	@Override
	public void saveOrder(Order order) {
		getSession().evict(order.getMember());
		getSession().merge(order);
	}

	@Override
	public List<Order> findAllProduct() {
		Criteria criteria = getSession().createCriteria(Order.class);
		return (List<Order>) criteria.list();
	}

	@Override
	public List<Order> findOrderByMid(String mId) {
		Criteria criteria = getSession().createCriteria(Order.class);
		criteria.add(Restrictions.eq("member.mId", mId));
		return (List<Order>) criteria.list();
	}
	
	@Override
	public List<Order> findOrderByMidAndCartTime(String mId, String cartTime) {
		Criteria criteria = getSession().createCriteria(Order.class);
		criteria.add(Restrictions.eq("member.mId", mId));
		criteria.add(Restrictions.eq("cart.cartTime", cartTime));
		return (List<Order>) criteria.list();
	}
	
	@Override
	public Order findOrderByMidAndCartTimeAndPNo(String mId, String cartTime, String pNo) {
		Criteria criteria = getSession().createCriteria(Order.class);
		criteria.add(Restrictions.eq("member.mId", mId));
		criteria.add(Restrictions.eq("cart.cartTime", cartTime));
		criteria.add(Restrictions.eq("product.pNo", pNo));
		return (Order) criteria.uniqueResult();
	}

	@Override
	public List<Order> findOrderByTransMid(String transMid) {
		Criteria criteria = getSession().createCriteria(Transaction.class);
		criteria.add(Restrictions.eq("transMid", transMid));
		
		List<Order> orderList = new ArrayList<Order>();
		
		for(Transaction trans : (List<Transaction>)criteria.list()){
//			Criteria criteria = getSession().createCriteria(Transaction.class);
//			criteria.add(Restrictions.eq("transMid", transMid));
		}
		
		return (List<Order>) criteria.list();
	}

	@Override
	public void deleteOrder(Integer id) {
		Query query = getSession().createSQLQuery(
				"delete from corder where seq = :seq");
		query.setInteger("seq", id);
		query.executeUpdate();
	}

	@Override
	public Order findByid(Integer id) {
		Criteria criteria = getSession().createCriteria(Order.class);
		criteria.add(Restrictions.eq("seq", id));
		return (Order) criteria.uniqueResult();
	}

	@Override
	public Order findByPNo(String pNo) {
		Criteria criteria = getSession().createCriteria(Order.class);
		criteria.add(Restrictions.eq("product.pNo", pNo));
		return (Order) criteria.uniqueResult();
	}

	@Override
	public void updateOrder(Order order) {
		getSession().update(order);
	}
	
}
