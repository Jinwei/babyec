package nsysu.edu.tw.dao;

import java.util.List;

import nsysu.edu.tw.model.table.Cart;


public interface CartDao {
	
	void saveCart(Cart cart);
    
    List<Cart> findAllCart();
    
    List<Cart> findCartByMId(String mId);
    
    Cart findCartByCartTime(String CartTime);
    
    Cart findCartByTNo(String tNo);
    
    Cart findNewCartByMid(String mId);
     
    void deleteRecord(Integer id);
     
    Cart findByid(Integer id);
    
    void updateCart(Cart cart);
}
