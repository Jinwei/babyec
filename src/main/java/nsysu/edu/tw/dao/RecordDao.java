package nsysu.edu.tw.dao;

import java.util.List;

import nsysu.edu.tw.model.table.Record;


public interface RecordDao {
	
	void saveCart(Record record);
    
    List<Record> findAllRecord();
    
    /**
     * 依產品編號查詢
     * @param pNo
     * @return
     */
    List<Record> findRecordByPNo(String pNo);
    
    /**
     * 依交易編號查詢
     * @param tNo
     * @return
     */
    List<Record> findRecordByTNo(String tNo);
     
    void deleteRecord(Integer id);
     
    Record findByid(Integer id);
    
    void updateOrder(Record record);
}
