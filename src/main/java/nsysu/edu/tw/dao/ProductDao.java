package nsysu.edu.tw.dao;

import java.util.List;

import nsysu.edu.tw.model.table.Product;


public interface ProductDao {
	
	void saveProduct(Product product);
    
    List<Product> findAllProduct();
    
    List<Product> findProductByMid(String mId);
    
    List<Product> findProductByCid(String cId);
    
    List<Product> findProductByBid(String bId);
     
    void deleteProduct(Integer id);
     
    Product findByid(Integer id);
    
    Product findByPNo(String pNo);
    
    void updateProduct(Product product);
}
