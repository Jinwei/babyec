package nsysu.edu.tw.dao;

import java.util.List;

import nsysu.edu.tw.model.table.Transaction;


public interface TransactionDao {
	
	void saveTransaction(Transaction transaction);
    
    List<Transaction> findAllTransaction();
    
    List<Transaction> findTransactionByTransMid(String transMid);
     
//    void deleteOrder(Integer id);
     
    Transaction findByid(Integer id);
    
    Transaction findByTNo(String tNo);
    
    void updateTransaction(Transaction transaction);
}
