package nsysu.edu.tw.dao;

import java.util.List;

import nsysu.edu.tw.model.table.Order;
import nsysu.edu.tw.model.table.Product;


public interface OrderDao {
	
	void saveOrder(Order order);
    
    List<Order> findAllProduct();
    
    List<Order> findOrderByMid(String mId);
    
    List<Order> findOrderByMidAndCartTime(String mId, String cartTime);
    
    Order findOrderByMidAndCartTimeAndPNo(String mId, String cartTime, String pNo);
    
    List<Order> findOrderByTransMid(String transMid);
     
    void deleteOrder(Integer id);
     
    Order findByid(Integer id);
    
    Order findByPNo(String pNo);
    
    void updateOrder(Order order);
}
