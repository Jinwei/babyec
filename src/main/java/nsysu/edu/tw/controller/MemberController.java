package nsysu.edu.tw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import nsysu.edu.tw.model.request.AddRegisterRequest;
import nsysu.edu.tw.model.request.CheckIdentityRequest;
import nsysu.edu.tw.model.response.AddRegisterResponse;
import nsysu.edu.tw.model.response.CheckIdentityResponse;
import nsysu.edu.tw.service.MemberService;

@Controller
public class MemberController {

	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value = "/addRegister", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<AddRegisterResponse> addRegister(
			@RequestBody AddRegisterRequest request) {

		return memberService.addRegister(request);
	}
	
	@RequestMapping(value = "/checkIdentity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CheckIdentityResponse> checkIdentity(
			@RequestBody CheckIdentityRequest request) {

		return memberService.checkIdentity(request);
	}
	
}
