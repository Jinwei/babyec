package nsysu.edu.tw.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nsysu.edu.tw.model.Hello;

@RestController
//@RequestMapping(value = "/hello")
public class HelloController {


	@RequestMapping(value = "/hello/{name}", method = RequestMethod.GET)
	@ResponseBody
	public Hello getPerson(@PathVariable String name) {
		Hello hello = new Hello();
		hello.setName("Hello " + name);
		return hello;
	}

	
	@RequestMapping(value = "/hello", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Hello savePersons(@RequestBody Hello hello) {
		hello.setName("Hello " + hello.getName());
		return hello;
	}
	
}
