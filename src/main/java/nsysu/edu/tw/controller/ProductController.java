package nsysu.edu.tw.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import nsysu.edu.tw.model.request.AddProductRequest;
import nsysu.edu.tw.model.response.AddProductResponse;
import nsysu.edu.tw.model.response.ProductListResponse;
import nsysu.edu.tw.model.response.ProductResponse;
import nsysu.edu.tw.service.ProductService;

@Controller
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@RequestMapping(value = "/addProduct", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<AddProductResponse> addRegister(
			@RequestBody AddProductRequest request) {

		return productService.addProduct(request);
	}
	
	@RequestMapping(value = "/product/{pNo}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ProductResponse> addRegister(
			@PathVariable String pNo) {

		return productService.getProduct(pNo);
	}
	
	@RequestMapping(value = "/getCidProductList/{cId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ProductListResponse> getCidProductList(
			@PathVariable String cId) {

		return productService.getCidProductList(cId);
	}
	
	@RequestMapping(value = "/getBidProductList/{bId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ProductListResponse> getBidProductList(
			@PathVariable String bId) {

		return productService.getBidProductList(bId);
	}
	
	@RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload( 
            @RequestParam("file") MultipartFile file){
			
//			String uploadPath = "C:\\Users\\Jinwei\\Desktop\\105EC\\workspace\\BabyEC\\src\\main\\webapp\\photos\\";
			String uploadPath = "/home/ubuntu/apache-tomcat-8.0.39/webapps/BabyEC/photos/";
			File uploadFolder = new File(uploadPath);
			if(!uploadFolder.exists()){//先判斷目錄存不存在
				uploadFolder.mkdirs();
			}
			String OriginName = file.getOriginalFilename();
			String randomName = UUID.randomUUID().toString().replaceAll("-", "");
            String fileName = randomName + OriginName.substring(OriginName.indexOf("."));
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = 
                        new BufferedOutputStream(new FileOutputStream(new File(uploadPath + fileName)));
                stream.write(bytes);
                stream.close();
                return "file url : http://54.238.128.52:8080/BabyEC/photos/" + fileName;
            } catch (Exception e) {
                return "You failed to upload ";
            }
        } else {
            return "You failed to upload " + OriginName + " because the file was empty.";
        }
    }
}
