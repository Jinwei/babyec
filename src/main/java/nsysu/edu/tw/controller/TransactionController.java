package nsysu.edu.tw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import nsysu.edu.tw.model.request.ConfirmRequest;
import nsysu.edu.tw.model.request.SaveTransactionRequest;
import nsysu.edu.tw.model.response.BuyerDataResponse;
import nsysu.edu.tw.model.response.BuyerListResponse;
import nsysu.edu.tw.model.response.SaveTransactionResponse;
import nsysu.edu.tw.model.response.SellerListResponse;
import nsysu.edu.tw.service.TransactionService;

@Controller
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@RequestMapping(value = "/getSellerList/{mId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<SellerListResponse> getSellerList(@PathVariable String mId) {
		return transactionService.getSellerList(mId);
	}
	
	@RequestMapping(value = "/getSellerAllList/{mId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<SellerListResponse> getSellerAllList(@PathVariable String mId) {
		return transactionService.getSellerAllList(mId);
	}

	@RequestMapping(value = "/saveTransaction", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<SaveTransactionResponse> saveTransaction(
			@RequestBody SaveTransactionRequest request) {
		return transactionService.saveTransaction(request);
	}
	
	@RequestMapping(value = "/getBuyerList/{mId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<BuyerListResponse> getCartList(
			@PathVariable String mId) {

		if(mId == null || "".equals(mId)){
			BuyerListResponse response = new BuyerListResponse();
			response.setStatus("Error");
			response.setMsg("或會員編號不能為空");
			return new ResponseEntity<BuyerListResponse>(response, HttpStatus.OK);
		}
		
		return transactionService.getBuyerList(mId);
	}
	
	@RequestMapping(value = "/comfirm", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BuyerDataResponse> comfirm(
			@RequestBody ConfirmRequest request) {
		return transactionService.comfirm(request);
		
	}
	
	@RequestMapping(value = "/refund", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BuyerDataResponse> refund(
			@RequestBody ConfirmRequest request) {
		return transactionService.refund(request);
		
	}
}
