package nsysu.edu.tw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import nsysu.edu.tw.model.request.CartRequest;
import nsysu.edu.tw.model.response.AddCartResponse;
import nsysu.edu.tw.model.response.CartListResponse;
import nsysu.edu.tw.service.CartService;
import nsysu.edu.tw.service.ProductService;

@Controller
public class CartController {

	
	@Autowired
	private CartService cartService;
	
	@RequestMapping(value = "/addCart", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<AddCartResponse> addCart(
			@RequestBody CartRequest request) {

		if(request.getpNo() == null || "".equals(request.getpNo())
				|| request.getmId() == null || "".equals(request.getmId())){
			AddCartResponse response = new AddCartResponse();
			response.setStatus("Error");
			response.setMsg("產品編號或會員編號不能為空");
			return new ResponseEntity<AddCartResponse>(response, HttpStatus.OK);
		}
		
		return cartService.addCart(request.getpNo(), request.getmId());
	}
	
	@RequestMapping(value = "/getCartList/{mId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<CartListResponse> getCartList(
			@PathVariable String mId) {

		if(mId == null || "".equals(mId)){
			CartListResponse response = new CartListResponse();
			response.setStatus("Error");
			response.setMsg("會員編號不能為空");
			return new ResponseEntity<CartListResponse>(response, HttpStatus.OK);
		}
		
		return cartService.getCartList(mId);
	}
	
	@RequestMapping(value = "/delCart", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CartListResponse> delCart(
			@RequestBody CartRequest request) {

		if(request.getpNo() == null || "".equals(request.getpNo())
				|| request.getmId() == null || "".equals(request.getmId())){
			CartListResponse response = new CartListResponse();
			response.setStatus("Error");
			response.setMsg("產品編號或會員編號不能為空");
			return new ResponseEntity<CartListResponse>(response, HttpStatus.OK);
		}
		
		return cartService.delCart(request.getpNo(), request.getmId());
	}
}
