package nsysu.edu.tw.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@ComponentScan("nsysu.edu.tw")
@EnableWebMvc
//@EnableWs
@PropertySource(value = "classpath:application.properties")
public class WebAppConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private Environment environment;
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
			.allowedOrigins("*")
			.allowedMethods("*")
			.allowedHeaders("*")
			.allowCredentials(false).maxAge(3600);
	}
	
	@Bean(name = "multipartResolver")
    public CommonsMultipartResolver multiPartResolver(){

        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        return resolver;
    }
	
	
//	@Bean(name = "CreateOrderRequest")
//	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema orderSchema) {
//		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
//		wsdl11Definition.setPortTypeName("OrderServicePort");
//		wsdl11Definition.setLocationUri("/soapws");
//		wsdl11Definition.setTargetNamespace("http://nsysu.edu.tw/soap");
//		wsdl11Definition.setSchema(orderSchema);
//		return wsdl11Definition;
//	}
//	@Bean
//	public XsdSchema studentsSchema() {
//		return new SimpleXsdSchema(new ClassPathResource("OrderService.xsd"));
//	}
	
}
