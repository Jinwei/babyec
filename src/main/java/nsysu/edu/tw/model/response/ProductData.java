package nsysu.edu.tw.model.response;

import java.io.Serializable;

public class ProductData implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2154195096892553608L;

	private String pNo;
	
	private String pName;

	private String pPicture;

	private String pDescription;

	private Integer unitPrice;
	
	private String catalog;

	private String brand;
	
	private String pMid;
	
	private String pMname;
	




	public String getpNo() {
		return pNo;
	}

	public void setpNo(String pNo) {
		this.pNo = pNo;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getpDescription() {
		return pDescription;
	}
	
	public void setpDescription(String pDescription) {
		this.pDescription = pDescription;
	}
	
	public String getpPicture() {
		return pPicture;
	}

	public void setpPicture(String pPicture) {
		this.pPicture = pPicture;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getpMid() {
		return pMid;
	}

	public void setpMid(String pMid) {
		this.pMid = pMid;
	}

	public String getpMname() {
		return pMname;
	}

	public void setpMname(String pMname) {
		this.pMname = pMname;
	}



}
