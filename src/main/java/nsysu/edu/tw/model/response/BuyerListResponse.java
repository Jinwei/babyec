package nsysu.edu.tw.model.response;

import java.util.List;

public class BuyerListResponse {

	private String status;
	
	private String msg;

	private List<BuyerData> buyerList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<BuyerData> getBuyerList() {
		return buyerList;
	}

	public void setBuyerList(List<BuyerData> buyerList) {
		this.buyerList = buyerList;
	}

	
}
