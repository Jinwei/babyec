package nsysu.edu.tw.model.response;

public class BuyerDataResponse {

	private String status;
	
	private String msg;

	private BuyerData buyerData;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public BuyerData getBuyerData() {
		return buyerData;
	}

	public void setBuyerData(BuyerData buyerData) {
		this.buyerData = buyerData;
	}


	
}
