package nsysu.edu.tw.model.response;

import nsysu.edu.tw.model.table.Product;

public class ProductResponse {

	private String status;
	
	private String msg;

	private ProductData product;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public ProductData getProduct() {
		return product;
	}

	public void setProduct(ProductData product) {
		this.product = product;
	}



	
}
