package nsysu.edu.tw.model.response;

import java.util.List;

import nsysu.edu.tw.model.table.Product;

public class ProductListResponse {

	private String status;
	
	private String msg;

	private List<Product> productList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	
	
}
