package nsysu.edu.tw.model.response;

import java.util.List;

public class SellerListResponse {

	private String status;
	
	private String msg;

	private List<SellerData> sellerList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<SellerData> getSellerList() {
		return sellerList;
	}

	public void setSellerList(List<SellerData> sellerList) {
		this.sellerList = sellerList;
	}
	
	
}
