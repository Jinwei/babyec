package nsysu.edu.tw.model.response;

import java.util.List;

import nsysu.edu.tw.model.table.Order;

public class CartListResponse {

	private String status;
	
	private String msg;

	private List<CartData> cartList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<CartData> getCartList() {
		return cartList;
	}

	public void setCartList(List<CartData> cartList) {
		this.cartList = cartList;
	}
	
	
}
