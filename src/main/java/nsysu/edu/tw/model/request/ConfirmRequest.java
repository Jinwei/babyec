package nsysu.edu.tw.model.request;

public class ConfirmRequest {

	private String mId;
	
	private String transMid;
	
	private String pNo;
	
	public String getmId() {
		return mId;
	}
	public void setmId(String mId) {
		this.mId = mId;
	}
	public String getTransMid() {
		return transMid;
	}
	public void setTransMid(String transMid) {
		this.transMid = transMid;
	}
	public String getpNo() {
		return pNo;
	}
	public void setpNo(String pNo) {
		this.pNo = pNo;
	}
	
	
}
