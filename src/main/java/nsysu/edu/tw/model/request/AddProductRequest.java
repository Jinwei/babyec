package nsysu.edu.tw.model.request;

public class AddProductRequest {

	private String mId;
	private String pCid;
	private String pBid;
	private String pName;
	private String pDesc;
	private Integer pPrice;
	private String pPicture;
	
	public String getmId() {
		return mId;
	}
	public void setmId(String mId) {
		this.mId = mId;
	}
	public String getpCid() {
		return pCid;
	}
	public void setpCid(String pCid) {
		this.pCid = pCid;
	}
	public String getpBid() {
		return pBid;
	}
	public void setpBid(String pBid) {
		this.pBid = pBid;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getpDesc() {
		return pDesc;
	}
	public void setpDesc(String pDesc) {
		this.pDesc = pDesc;
	}
	public Integer getpPrice() {
		return pPrice;
	}
	public void setpPrice(Integer pPrice) {
		this.pPrice = pPrice;
	}
	public String getpPicture() {
		return pPicture;
	}
	public void setpPicture(String pPicture) {
		this.pPicture = pPicture;
	}
	
	
	
	
}
