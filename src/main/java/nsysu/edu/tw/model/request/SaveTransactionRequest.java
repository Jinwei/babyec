package nsysu.edu.tw.model.request;

import java.util.List;

public class SaveTransactionRequest {

	private String mId;

	private List<String> pNo;
	
	private String pStore;

	public String getmId() {
		return mId;
	}
	
	public void setmId(String mId) {
		this.mId = mId;
	}

	public List<String> getpNo() {
		return pNo;
	}

	public void setpNo(List<String> pNo) {
		this.pNo = pNo;
	}

	public String getpStore() {
		return pStore;
	}

	public void setpStore(String pStore) {
		this.pStore = pStore;
	}
	

	
	
}
