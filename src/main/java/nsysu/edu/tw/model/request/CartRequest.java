package nsysu.edu.tw.model.request;

public class CartRequest {

	private String pNo;
	
	private String mId;

	public String getpNo() {
		return pNo;
	}

	public void setpNo(String pNo) {
		this.pNo = pNo;
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}
	
	
	
}
