package nsysu.edu.tw.model.table;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "transaction", indexes={
		@Index(columnList = "tNo", name = "idx_tno"),
		@Index(columnList = "transTime", name = "idx_transTime")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1724450838249691405L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seq", nullable = false)
	private int seq;

	@Column(name = "tNo", nullable = false, length = 10)
	private String tNo;

	@Column(name = "transTime", nullable = false, length = 20)
	private String transTime;

	@Column(name = "transLocation", nullable = false, length = 20)
	private String transLocation;

	@Column(name = "transStatus", nullable = false, length = 5)
	private String transStatus;

//	@Column(name = "transMid", nullable = false, length = 10)
//	private String transMid;
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, targetEntity = Member.class)
	@JoinColumn(name = "transMid", referencedColumnName = "mId")
	private Member member;
	
	private String pStore;

	@OneToOne(mappedBy = "transaction")
	private Cart cart;
	
	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String gettNo() {
		return tNo;
	}

	public void settNo(String tNo) {
		this.tNo = tNo;
	}

	public String getTransTime() {
		return transTime;
	}

	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}

	public String getTransLocation() {
		return transLocation;
	}

	public void setTransLocation(String transLocation) {
		this.transLocation = transLocation;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

//	public String getTransMid() {
//		return transMid;
//	}
//
//	public void setTransMid(String transMid) {
//		this.transMid = transMid;
//	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		this.member = member;
	}

	public Cart getCart() {
		return cart;
	}


	public void setCart(Cart cart) {
		this.cart = cart;
	}

	public String getpStore() {
		return pStore;
	}

	public void setpStore(String pStore) {
		this.pStore = pStore;
	}
	


}
