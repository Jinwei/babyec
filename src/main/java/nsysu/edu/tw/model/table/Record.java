package nsysu.edu.tw.model.table;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "record", indexes={
		@Index(columnList = "pNo", name = "idx_pno"),
		@Index(columnList = "tNo", name = "idx_tno")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Record implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1833264666265093307L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seq", nullable = false)
	private int seq;

//	@Column(name = "tNo", nullable = false, length = 10)
//	private String tNo;
	
	@Column(name = "pNo", nullable = false, length = 10)
	private String pNo;

	@Column(name = "amount")
	private Integer amount;

	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, targetEntity = Transaction.class)
	@JoinColumn(name = "tNo", referencedColumnName="tNo")
	private Transaction transaction;

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

//	public String gettNo() {
//		return tNo;
//	}
//
//	public void settNo(String tNo) {
//		this.tNo = tNo;
//	}

	public String getpNo() {
		return pNo;
	}

	public void setpNo(String pNo) {
		this.pNo = pNo;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}


}
