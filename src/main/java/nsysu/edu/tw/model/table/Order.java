package nsysu.edu.tw.model.table;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "corder", indexes={
		@Index(columnList = "pNo", name = "idx_pno"),
		@Index(columnList = "cartTime", name = "idx_carttime"),
		@Index(columnList = "oMid", name = "idx_mid")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1279967975666059204L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seq", nullable = false)
	private int seq;

//	@Column(name = "oMid", nullable = false, length = 10)
//	private String oMid;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Member.class)
	@JoinColumn(name = "oMid", referencedColumnName = "mId")
	private Member member;
	
//	@Column(name = "cartTime", nullable = false, length = 20)
//	private String cartTime;

//	@Column(name = "pNo", nullable = false, length = 10)
//	private String pNo;

	@Column(name = "amount")
	private Integer amount;
	
	@Column(name = "oPrice")
	private Integer oPrice;

	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, targetEntity = Cart.class)
	@JoinColumn(name = "cartTime", referencedColumnName="cartTime")
	private Cart cart;
	
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, targetEntity = Product.class)
	@JoinColumn(name = "pNo", referencedColumnName="pNo")
	private Product product;
	
	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	
//	public String getoMid() {
//		return oMid;
//	}
//
//	public void setoMid(String oMid) {
//		this.oMid = oMid;
//	}

//	public String getCartTime() {
//		return cartTime;
//	}
//
//	public void setCartTime(String cartTime) {
//		this.cartTime = cartTime;
//	}

//	public String getpNo() {
//		return pNo;
//	}
//
//	public void setpNo(String pNo) {
//		this.pNo = pNo;
//	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getoPrice() {
		return oPrice;
	}

	public void setoPrice(Integer oPrice) {
		this.oPrice = oPrice;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}



}
