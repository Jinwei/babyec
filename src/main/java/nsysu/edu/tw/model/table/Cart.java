package nsysu.edu.tw.model.table;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "cart", indexes={
		@Index(columnList = "tNo", name = "idx_tno"),
		@Index(columnList = "mId", name = "idx_mid")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cart implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9166510549034659635L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seq", nullable = false)
	private int seq;

//	@Column(name = "tNo", nullable = false, length = 10)
//	private String tNo;
	
//	@Column(name = "mId", nullable = false, length = 10)
//	private String mId;

	@Column(name = "cartTime", nullable = false, length = 20)
	private String cartTime;

	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, targetEntity = Member.class)
	@JoinColumn(name = "mId", referencedColumnName="mId")
	private Member member;
	
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, targetEntity = Transaction.class)
	@JoinColumn(name = "tNo", referencedColumnName="tNo")
	private Transaction transaction;
	
	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

//	public String gettNo() {
//		return tNo;
//	}
//
//	public void settNo(String tNo) {
//		this.tNo = tNo;
//	}

//	public String getmId() {
//		return mId;
//	}
//
//	public void setmId(String mId) {
//		this.mId = mId;
//	}

	public String getCartTime() {
		return cartTime;
	}

	public void setCartTime(String cartTime) {
		this.cartTime = cartTime;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}


}
