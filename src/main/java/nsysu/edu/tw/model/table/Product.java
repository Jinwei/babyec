package nsysu.edu.tw.model.table;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "product", indexes={
		@Index(columnList = "pNo", name = "idx_pno"),
		@Index(columnList = "pMid", name = "idx_mid")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1241680159205056367L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seq", nullable = false)
	private long seq;

	@Column(name = "pNo", nullable = false, length = 10)
	private String pNo;
	
	@Column(name = "pName", nullable = false, length = 20)
	private String pName;

	@Column(name = "pPicture", nullable = false, length = 255)
	private String pPicture;

	@Column(name = "pDescription", nullable = false, length = 4096)
	private String pDescription;

	@Column(name = "unitPrice")
	private Integer unitPrice;
	
	@Column(name = "catalog", nullable = false, length = 5)
	private String catalog;

	@Column(name = "brand", nullable = false, length = 5)
	private String brand;
	
//	@Column(name = "pMid", nullable = false, length = 10)
//	private String pMid;
	
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, targetEntity = Member.class)
	@JoinColumn(name = "pMid", referencedColumnName = "mId")
	private Member member;


	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}


	public String getpNo() {
		return pNo;
	}

	public void setpNo(String pNo) {
		this.pNo = pNo;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getpDescription() {
		return pDescription;
	}
	
	public void setpDescription(String pDescription) {
		this.pDescription = pDescription;
	}
	
	public String getpPicture() {
		return pPicture;
	}

	public void setpPicture(String pPicture) {
		this.pPicture = pPicture;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

//	public String getpMid() {
//		return pMid;
//	}
//
//	public void setpMid(String pMid) {
//		this.pMid = pMid;
//	}


}
